﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Enemy : MonoBehaviour {

    public GameObject target;
    public float attackDistance = 3.5f;
    NavMeshAgent agent;
    int lehetoseg;
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        if(target == null)
        {
            target = GameObject.FindGameObjectWithTag("Pacman");
        }
    }
	
	// Update is called once per frame
	void Update () {
        agent.destination = target.transform.position;

        if(Vector3.Distance(target.transform.position, transform.position) < attackDistance)
        {
            if (lehetoseg < 100)
            {
                lehetoseg++;
                proba_Player_script ctrl = target.GetComponent<proba_Player_script>();
                ctrl.Attack();
            }
            else
            {
                SceneManager.LoadScene("LOSE");
            }
        }
	}
}
