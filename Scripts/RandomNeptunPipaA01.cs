﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Pacman_University
{
    public class RandomNeptunPipaA01 : MonoBehaviour
    {

        public GameObject Neptun_pipa;
        public GameObject player;
        public GameObject quest1;
        public GameObject quest2;
        public GameObject quest3;
        public GameObject quest4;


        public float minX;
        public float maxX;
        public float minZ;
        public float maxZ;

        public int szamol;
        public bool questOpened;
       

        protected Vector3 position;

        // Use this for initialization
        void Start()
        {
            minX = -20.0f;
            maxX = 20.0f;
            minZ = -12.5f;
            maxZ = 12.5f;

            player = GameObject.FindGameObjectWithTag("Pacman");
            Neptun_pipa = GameObject.FindGameObjectWithTag("Neptun_pipa");
            quest1 = GameObject.FindGameObjectWithTag("Question1");
            quest2 = GameObject.FindGameObjectWithTag("Question2");
            quest3 = GameObject.FindGameObjectWithTag("Question3");
            quest4 = GameObject.FindGameObjectWithTag("Question4");

            quest1.SetActive(false);
            quest2.SetActive(false);
            quest3.SetActive(false);
            quest4.SetActive(false);
           
            szamol = 0;
            

        }
        
        Vector3 SpawnNeptunPipa()
        {
            float x = UnityEngine.Random.Range(minX, maxX);
            float y = 0.5f;
            float z = UnityEngine.Random.Range(minZ, maxZ);
            return new Vector3(x, y, z);
        }

        public void Spawn()
        {
            position.x = UnityEngine.Random.Range(minX, maxX);
            position.y = 0.5f;
            position.z = UnityEngine.Random.Range(minZ, maxZ);
            Instantiate(Neptun_pipa, position, Quaternion.identity);
            CancelInvoke();
        }

        void OnTriggerEnter(Collider c)
        {
            if (c.gameObject.CompareTag("Pacman"))
            {
                
                if (szamol < 4)
                {
                    position = SpawnNeptunPipa();
                    Neptun_pipa.transform.position = position;
                    switch (szamol){
                        case 0:
                            questOpened = true;
                            Szunet();
                            quest1.SetActive(true);
                            break;
                        case 1:
                            questOpened = true;
                            Szunet();
                            quest2.SetActive(true);
                            break;
                        case 2:
                            questOpened = true;
                            Szunet();
                            quest3.SetActive(true);
                            break;
                        case 3:
                            questOpened = true;
                            Szunet();
                            quest4.SetActive(true);
                            break;
                    }
                    

                }
                
            }
                
        }
        public void Good()
        {
            quest1.SetActive(false);
            quest2.SetActive(false);
            quest3.SetActive(false);
            quest4.SetActive(false);
            questOpened = false;
            Szunet();
            szamol++;
            proba_Player_script ctrl = player.GetComponent<proba_Player_script>();
            ctrl.Pipa(szamol);
            switch (szamol)
            {
                case 1:
                    GameObject.Destroy(GameObject.FindGameObjectWithTag("T1"));
                    break;
                case 2:
                    GameObject.Destroy(GameObject.FindGameObjectWithTag("T2"));
                    break;
                case 3:
                    GameObject.Destroy(GameObject.FindGameObjectWithTag("T3"));
                    break;
                case 4:
                    GameObject.Destroy(GameObject.FindGameObjectWithTag("T4"));
                    SceneManager.LoadScene("WINA01");
                    break;
            }
        }
        public void Bad()
        {
            quest1.SetActive(false);
            quest2.SetActive(false);
            quest3.SetActive(false);
            quest4.SetActive(false);
            questOpened = false;
            proba_Player_script ctrl = player.GetComponent<proba_Player_script>();
            ctrl.Mistake();
            Szunet();
        }
        public void Szunet()
        {
            Controller pause = Neptun_pipa.GetComponent<Controller>();
            pause.Pause();
            
        }
        // Update is called once per frame
        void Update()
        {
            
            /*if (Vector3.Distance(player.transform.position, Neptun_pipa.transform.position) < 2.0f)
            {
                
                if (szamol < 4)
                {
                    Time.timeScale = 0;
                    position = SpawnNeptunPipa();
                    Neptun_pipa.transform.position = position;
                    proba_Player_script ctrl = player.GetComponent<proba_Player_script>();
                    ctrl.Pipa(szamol);
                    szamol++;
                    
                }
                else
                {
                    SceneManager.LoadScene("WINA01");
                }
            }*/
        }
        
    }
}
