﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using Pacman_University;

public class proba_Player_script : MonoBehaviour
{

	public float height = 10;
	public float range = 2;

	public Text Health;
	private float currentHealth;

	public Text Score;
	public int currentScore;

    private Rigidbody rigidBody;
	private Collider floorCollider;
	
	private NavMeshAgent navMeshAgent;

    // Use this for initialization
    void Start()
	{

		currentHealth = 100;
		SetHealthText ();

		currentScore = 0;
		SetCurrentScore ();
        
        navMeshAgent = GetComponent<NavMeshAgent>();

        rigidBody = GetComponent<Rigidbody>();
		if (rigidBody == null)
		{
			Debug.Log("Rigidbody is not attached to the object!");
		}
		
		GameObject floor = GameObject.Find("Floor");
		if (floor)
		{
			floorCollider = floor.GetComponent<Collider>();
			if (floorCollider == null)
			{
				Debug.Log("Could not find the Collider component of the Floor GameObject");
			}
		}
		else
		{
			Debug.Log("Could not find the Collider component of the Floor GameObject");
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		{
			float vertical = Input.GetAxis("Vertical");
			float horizontal = Input.GetAxis("Horizontal");
			
			Vector3 walkingDirection = 
				new Vector3(horizontal, 0.0f, vertical);
			navMeshAgent.SetDestination(
				transform.position + walkingDirection);
		}
	}
	
	void OnTriggerEnter(Collider c)
	{
		/*Debug.Log("Pacman.OnTriggerEnter");


		if (c.gameObject.CompareTag("Neptun_pipa")) {
            
            currentScore = currentScore + 1;
			Score.text = "Count " + currentScore.ToString ();
			SetCurrentScore();
            

		}*/

	}
    
    public void LOSE () {
		SceneManager.LoadScene("LOSE");
	}



	void OnTriggerStay(Collider c)
	{
		Debug.Log("Pacman.OnTriggerStay");
	}
    public void Mistake()
    {
        currentHealth -=15;
        if (currentHealth < 0)
        {
            currentHealth = 0;
            SetHealthText();
            SceneManager.LoadScene("LOSE");
        }
        SetHealthText();
    }

	public void Attack(){
		
		currentHealth--;
		SetHealthText ();
		
		if(currentHealth == 0){
			GameObject.Destroy (this.gameObject);
			SceneManager.LoadScene("LOSE");
			
		}
		
	}
    public void Pipa(int n)
    {

        currentScore = n;

        SetCurrentScore();

    }

    void SetHealthText(){
		Health.text = "Félév teljesíthetősége: " + currentHealth.ToString () + " %";
	}
	

	public void SetCurrentScore(){
		Score.text = "Aláírás: " + currentScore.ToString () + "/4";
	}




}
