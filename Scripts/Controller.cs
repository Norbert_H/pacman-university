﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Controller : MonoBehaviour {

	public bool paused;
	public Text PAUSE;

	// Use this for initialization
	void Start () {

		SetPauseText ();
		SetPauseText2 ();
	}
	public void Ido()
    {
        if (paused)
        {
            Time.timeScale = 0;
            SetPauseText();
        }
        else if (!paused)
        {
            Time.timeScale = 1;
            SetPauseText2();
        }
    }
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.P)){
			paused = !paused;
            Ido();
		}
	}

	void SetPauseText(){
		PAUSE.text = "Game Paused";
	}

	void SetPauseText2(){
		PAUSE.text = "";
	}

	public void Pause(){
			paused =! paused;

			if (paused){
				Time.timeScale = 0;
				SetPauseText ();
			}
			else if(!paused){
				Time.timeScale = 1;
				SetPauseText2();
			}
	}
}
