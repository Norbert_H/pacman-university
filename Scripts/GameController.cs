﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour {

	// Use this for initialization
	/*void Start () {
	
	}*/
	
	// Update is called once per frame
	public void StartGame (string sceneName) {
        SceneManager.LoadScene(sceneName);
		
	}

    public void ExitGame()
    {
        Application.Quit();
    }
	public void Update(){
		
	}
}
